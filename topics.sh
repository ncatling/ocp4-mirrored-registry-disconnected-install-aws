# workaround

# check status of mirror daemon
$ systemctl status mirror-registry


# update name of the container
$ vim /etc/systemd/system/mirror-registry.service

$ podman stop mirror-registry
$ podman rm mirror-registry
$ systemctl reload mirror-registry
$ systemctl start mirror-registry


# discovering the repositoires in the registry
$ curl -u <admin_user>:<admin_password> -X  GET https://{FQDN}}:5000/v2/_catalog

# list existing images at local registry
$ curl -u <admin_user>:<admin_password> https://{FQDN}:5000/v2/ocp4/openshift4/tags/list | jq

# pull the images from local registry
$ podman pull {FQDN}:5000/ocp4/openshift4:4.3.0-kuryr-cni
